Train & Run the NLU Server:
python -m rasa_nlu.train -c config.yml -d data/intents --path models/weather_nlu
python -m rasa_nlu.server --path models/weather_nlu --response_log data -w data

Train & Run the Core Server:
To train the bot run: python train_bot.py
python -m rasa_core.server -d models/dialogue -u models/weather_nlu/weatherbot/current -o out.log