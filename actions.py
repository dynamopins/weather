﻿from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

from rasa_core.actions.forms import FormAction, EntityFormField
from rasa_core.events import SlotSet

import os
import requests


class ActionWeather(FormAction):
    
    def name(self):
        return 'action_weather'
    
    def run(self, dispatcher, tracker, domain):
        node_url = 'http://localhost:1880/form'
        payload = {}
        loc = tracker.get_slot('location')
        time = tracker.get_slot('time')
        payload['location'] = loc
        payload['time'] = time
        payload['formName'] = 'weather'
        r = requests.get(node_url, params=payload)
        response_json = r.json()
        print(response_json)
        
        if (response_json['allSlotsFilled']):
            current = response_json['response']
            print(current['location']['name'])
            city = current['location']['name']
            condition = current['current']['condition']['text']
            temperature_c = current['current']['temp_c']
            dispatcher.utter_message("""Momentan ist es {} in {}. Die Temperatur beträgt {} Grad.""" \
                                     .format(condition, city, temperature_c))
        else:
            dispatcher.utter_message(response_json['response'])
        
        return [SlotSet('location', loc), SlotSet('time', time)]
