## Generated Story -9065560235133120415
* greet
    - utter_greet
* inform
    - utter_ask_location
* inform{"location": "italy"}
    - slot{"location": "italy"}
    - action_weather
* goodbye
    - utter_goodbye
    - export
## Generated Story -9065560235133120416
* greet
    - utter_greet
* inform{"location": "italy"}
    - slot{"location": "italy"}
    - action_weather
* goodbye
    - utter_goodbye
    - export
## Generated Story -9065560235133120416
* greet
    - utter_greet
* inform{"time": "today"}
    - slot{"location": "today"}
    - action_weather
* goodbye
    - utter_goodbye
    - export
## Generated Story -9065560235133120416
* greet
    - utter_greet
* inform{"time": "today", "location": "italy"}
    - slot{"location": "today", "location": "italy"}
    - action_weather
* goodbye
    - utter_goodbye
    - export